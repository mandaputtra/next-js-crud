require('dotenv').config()

import knex from 'knex'

const pg = knex({
  client: 'pg',
  connection: process.env.DATABASE_URL,
});


export default function handler(req, res) {
  switch (req.method) {
    case 'GET':
      return getAllData()
    case 'POST':
      return create(req.body)
    case 'PUT':
      return update(req.body, req.params.id)
    case 'DELETE':
      return deleteItem(req.params.id)
    default:
      return res.status(405).json({ message: 'Method not allowed'})
  }

  async function getAllData() {
    const products = await pg('products').select('*')
    return res.status(200).json(products)
  }

  async function create(body) {
    const product = await pg('products').insert(body).returning('*')
    return res.status(200).json(product)
  }

  async function update(body, id) {
    const product = await pg('products').where('id', '=', id).update(body).returning('*')
    return res.status(200).json(product)
  }

  async function deleteItem(id) {
    await pg('products').where('id', '=', id).del()
    return res.status(200).json({ message: 'Success delete product' })
  }
}
