import { TextInput, Button, Group, Box } from '@mantine/core';
import { useForm } from '@mantine/form';
import { useEffect, useState } from 'react';
import { FetchApi } from '../utils/fetch'
import ProductTable from '../components/tables'

export default function Home() {
  const fetch = new FetchApi('/api/products')

  const [productItems, setProductItems] = useState([])

  const form = useForm({
    initialValues: {
      name: '',
      category: '',
      price: 0,
    },
  });

  async function create(values) {
    const response = await fetch.post(values)
    console.log(response)
  }

  // async function update(values, id) {
  //   const response = fetch.update(values, id)
  // }

  async function getAll() {
    const response = await fetch.getAll()
    setProductItems(response)
  }

  useEffect(() => {
    async function fetchData() {
      getAll()
    }
    fetchData()
  }, [])

  return (
    <Box mt={10} sx={{ maxWidth: 300 }} mx="auto">
      <form onSubmit={form.onSubmit((values) => create(values))}>
        <TextInput
          required
          label="Name"
          placeholder="Nama barang"
          {...form.getInputProps('name')}
        />


        <TextInput
          required
          label="Category"
          placeholder="Kategori barang"
          {...form.getInputProps('category')}
        />

        <TextInput
          required
          type="number"
          label="Price"
          placeholder="Harga barang, (ex: 1000000)"
          {...form.getInputProps('price')}
        />

        <Group position="right" mt="md">
          <Button type="submit">Submit</Button>
        </Group>
      </form>

      <ProductTable data={productItems} />
    </Box>
  );
}
