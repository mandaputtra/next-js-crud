import {
  Table,
  Text,
  ScrollArea,
} from '@mantine/core';
import React from 'react';

export default function ProductTable({ data }) {
  const rows = data.map((item) => (
    <tr key={item.id}>
      <td>
        <Text>{item.name}</Text>
      </td>
      <td>
        <Text>{item.category}</Text>
      </td>
      <td>
        <Text>{item.price}</Text>
      </td>
    </tr>
  ));

  return (
    <ScrollArea>
      <Table sx={{ maxWidth: 300 }} verticalSpacing="sm">
        <thead>
          <tr>
            <th>Product Name</th>
            <th>Category</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>{rows}</tbody>
      </Table>
    </ScrollArea>
  );
}
