import fetch from 'isomorphic-fetch'

export class FetchApi {
  constructor(url) {
    this.url = url
  }

  async post(body) {
    const response = await fetch(this.url, {
      credentials: 'same-origin', // 'include', default: 'omit'
      method: 'POST',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    })
    
    return this._handleResponse(response)
  }

  async getAll() {
    const response = await fetch(this.url, {
      credentials: 'same-origin', // 'include', default: 'omit'
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
    
    return this._handleResponse(response)
  }

  async update(body, id) {
    const response = await fetch(`${this.url}/${id}`, {
      credentials: 'same-origin', // 'include', default: 'omit'
      method: 'PUT',
      body: JSON.stringify(body),
      headers: { 'Content-Type': 'application/json' },
    })
    
    return this._handleResponse(response)
  }

  async update(id) {
    const response = await fetch(`${this.url}/${id}`, {
      credentials: 'same-origin', // 'include', default: 'omit'
      method: 'DELETE',
      headers: { 'Content-Type': 'application/json' },
    })
    return this._handleResponse(response)
  }

  async _handleResponse(response) {
    const json = await response.json()
    if (response.ok) {
      return json
    } else {
      console.error(json)
    }
  }
}
